﻿using UnityEngine;
using System.Collections.Generic;

using LudumDare.Managers;

namespace LudumDare.Factories
{
    public class PrefabFactory : MonoBehaviour
    {
        public GameObject[] Prefabs;

        private Dictionary<string, GameObject> _prefabs;

        //  PUBLIC

        public GameObject CreatePrefab(string name)
        {
            D.Trace("[PrefabFactory] CreatePrefab ( name:{0} )", name);
            return CreatePrefab(name, transform.position);
        }

        public GameObject CreatePrefab(string name, Vector3 position)
        {
            D.Trace("[PrefabFactory] CreatePrefab ( name:{0}, pos:{1} )", name, position);
            return createPrefab(name, position);
        }

        //  PRIVATE

        private GameObject createPrefab(string name, Vector3 position)
        {
            D.Trace("[PrefabFactory] createPrefab ( name:{0}, pos:{1} )", name, position);

            if (_prefabs.ContainsKey(name))
            {
                GameObject g = (GameObject)Instantiate(_prefabs[name], position, Quaternion.identity);
                g.name = string.Format("[Prefab] {0}", name);
                return g;
            }
            else
            {
                D.Warn("- prefab {0} does not exist", name);
                GameObject g = new GameObject();
                g.name = string.Format("[ERROR] !! MissingPrefab({0}) !!", name);
                return g;
            }
        }

        //  MONO

        void Start()
        {
            D.Trace("[PrefabFactory] Start");
        }

        void OnEnable()
        {
            D.Trace("[PrefabFactory] OnEnable");
            DontDestroyOnLoad(gameObject);
        }

        private void Awake()
        {
            D.Trace("[PrefabFactory] Awake");
            GameManager.Instance.Prefabs = this;
            _prefabs = new Dictionary<string, GameObject>();
            foreach (GameObject g in Prefabs)
            {
                _prefabs.Add(g.name, g);
            }
        }

    }
}