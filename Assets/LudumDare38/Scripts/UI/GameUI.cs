﻿using LudumDare.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    public Text ScoreText;
    public Text HiScoreText;
    public Sprite HealthSprite;
    public GameObject HealthPosition;

    private GameManager _gameManager;
        
	// Use this for initialization
	void Update ()
    {
        ScoreText.text = _gameManager.Score.ToString();
        HiScoreText.text = _gameManager.HiScore.ToString();

	}

    private void OnEnable()
    {
        _gameManager = GameManager.Instance;
    }
}
