﻿using LudumDare.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomePanel : GamePanel
{
    public Text TextTitle;
    public Text TextMessage;
    public Text TextCopyright;
    public Text TextVersion;

    public void StartNewGame()
    {
        D.Trace("[HomePanel] StartNewGame");
        _gameManager.StartNewGame();
    }

    public void ContinueGame()
    {
        D.Trace("[HomePanel] ContinueGame");
        _gameManager.ContinueGame();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public override void ShowPanel()
    {
        D.Trace("[HomePanel] ShowPanel");
        TextTitle.text = _gameManager.Game.Title;
        TextMessage.text = _gameManager.Game.Message;
        TextCopyright.text = _gameManager.Game.Copyright;
        TextVersion.text = _gameManager.Game.Version;
        base.ShowPanel();
    }

}
