﻿using LudumDare.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePanel : MonoBehaviour
{
    protected GameManager _gameManager;

    public virtual void ShowPanel()
    {

    }

    private void OnEnable()
    {
        _gameManager = GameManager.Instance;
    }
}
