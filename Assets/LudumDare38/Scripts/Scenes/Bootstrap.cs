﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bootstrap : MonoBehaviour
{
    public string HomeScene;

	void Start ()
    {
        SceneManager.LoadScene(HomeScene);	    	
	}
}
