﻿using LudumDare.Data;
using LudumDare.Managers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace LudumDare.Scenes
{
    public class Home : MonoBehaviour
    {
        public GameData DefaultGame;
        public GameSettingsData DefaultGameSettings;
        public Text TextTitle;
        public Text TextMessage;
        public Text TextCopyright;
        public Text TextVersion;

        public void OpenScene(string name)
        {
            SceneManager.LoadScene(name);
        }

        private void OnEnable()
        {
            D.Trace("[Home] OnEnable");
            GameManager.Instance.Game = DefaultGame;
            GameManager.Instance.GameSettings = DefaultGameSettings;

            GameManager.Instance.GameSettings = GameManager.Instance.LoadGameSettings(GameManager.Instance.GameSettings);

            if (GameManager.Instance.GameSettings.LastSave > 0)
            {
                GameManager.Instance.PlayerSettings = GameManager.Instance.LoadPlayer(GameManager.Instance.GameSettings.LastSave);
            }

            TextTitle.text = GameManager.Instance.Game.Title;
            TextMessage.text = GameManager.Instance.Game.Message;
            TextCopyright.text = GameManager.Instance.Game.Copyright;
            TextVersion.text = GameManager.Instance.Game.Version;
        }

    }
}