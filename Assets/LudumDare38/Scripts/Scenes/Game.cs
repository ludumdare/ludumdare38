﻿using LudumDare.Controllers;
using LudumDare.Data;
using LudumDare.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace LudumDare.Scenes
{
    public class Game : MonoBehaviour
    {
        public GameData DefaultGame;
        public GameSettingsData DefaultGameSettings;

        public GamePanel HomePanel;
        public GamePanel AboutPanel;
        public GamePanel MedalsPanel;
        public GamePanel WinPanel;
        public GamePanel OverPanel;
        public GamePanel SettingsPanel;
        public GamePanel GamePanel;

        public Text DebugText;

        private GameController _gameController;
        private GameManager _gameManager;
        private Camera _gameCamera;

        public int StartSystem;
        public int StartWorld;
        public bool JumpStart;

        public void Home()
        {
            _gameManager.GameState = GameStates.GAME_HOME;
            GameHelper.ReloadCurrentScene();
        }

        private void showPanel(GamePanel panel)
        {
            D.Trace("[Game] showPanel");
            hidePanels();
            panel.gameObject.SetActive(true);
            panel.ShowPanel();
        }

        private void hidePanels()
        {
            D.Trace("[Game] hidePanels");
            HomePanel.gameObject.SetActive(false);
            AboutPanel.gameObject.SetActive(false);
            MedalsPanel.gameObject.SetActive(false);
            WinPanel.gameObject.SetActive(false);
            OverPanel.gameObject.SetActive(false);
            SettingsPanel.gameObject.SetActive(false);
            GamePanel.gameObject.SetActive(false);
        }

        private void openState()
        {
            D.Trace("[Game] openState");

            if (_gameManager.GameState == GameStates.GAME_RUNNING)
            {
                showPanel(GamePanel);
                continueGame();
            }

            if (_gameManager.GameState == GameStates.GAME_OVER)
            {
                showPanel(OverPanel);
                return;
            }

            if (_gameManager.GameState == GameStates.GAME_WIN)
            {
                showPanel(WinPanel);
                return;
            }

            if (_gameManager.GameState == GameStates.GAME_ABOUT)
            {
                showPanel(AboutPanel);
                return;
            }

            if (_gameManager.GameState == GameStates.GAME_SETTINGS)
            {
                showPanel(SettingsPanel);
                return;
            }

            if (_gameManager.GameState == GameStates.GAME_HOME)
            {
                showPanel(HomePanel);
            }

            if ((_gameManager.GameState != GameStates.GAME_RUNNING) && JumpStart)
            {
                _gameManager.Lives = 1;
                _gameManager.Score = 0;
                _gameManager.HiScore = 0;
                _gameManager.CurrentSystem = StartSystem;
                _gameManager.CurrentWorld = StartWorld;
                _gameManager.GameState = GameStates.GAME_RUNNING;
                GameHelper.ReloadCurrentScene();
            }

        }

        private void continueGame()
        {
            D.Trace("[Game] continueGame");
            _gameController.StartController();
            _gameController.StartWorld(_gameManager.CurrentWorld);
        }

        private void Start()
        {
            D.Trace("[Game] Start");
            hidePanels();
            openState();
        }

        private void OnEnable()
        {
            D.Trace("[Game] OnEnable");

            _gameController = GameObject.FindObjectOfType<GameController>();
            _gameManager = GameManager.Instance;
            _gameCamera = Camera.main;

            _gameManager.Game = DefaultGame;
            _gameManager.GameSettings = DefaultGameSettings;

            _gameManager.GameSettings = _gameManager.LoadGameSettings(_gameManager.GameSettings);

            if (_gameManager.GameSettings.LastSave > 0)
            {
                _gameManager.PlayerSettings = _gameManager.LoadPlayer(_gameManager.GameSettings.LastSave);
            }
        }

        private void Update()
        {
            if (_gameManager.GameController.World == null)
                return;

            string debug = "";
            debug += "\nSystem: " + _gameManager.GameController.System.name;
            debug += "\nWorld:" + _gameManager.GameController.World.name;
            debug += "\nTurn:" + _gameManager.GameController.Turn.TurnNumber;
            debug += "\nActive:" + _gameManager.GameController.Turn.ActiveTurnable;
            debug += "\nAlive:" + _gameManager.GameController.Player.Player.IsAlive;
            debug += "\nAvailable:" + _gameManager.GameController.World.TotalResources;
            debug += "\nCollected:" + _gameManager.GameController.CollectedResources.Count;
            debug += "\nLives:" + _gameManager.Lives;
            debug += "\nScore:" + _gameManager.Score;
            debug += "\nHiScore:" + _gameManager.HiScore;
            DebugText.text = debug;
        }
    }
}