﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game;
using LudumDare.Managers;

namespace LudumDare.Controllers
{
    public class TurnController : Controller
    {
        public Player Player { get; set; }

        private List<Turnable> _turnables;
        private List<Turnable> _sortedTurnables;
        private int _turnNumber;
        private string _activeTurnable;
        private Coroutine _runner;

        public string ActiveTurnable
        {
            get { return _activeTurnable; }
        }

        public int TurnNumber
        {
            get { return _turnNumber; }
        }

        public override void InitController()
        {
            D.Trace("[TurnController] InitController");
            _turnables = new List<Turnable>();
        }

        public override void StartController()
        {
            D.Trace("[TurnController] StartController");
            _turnNumber = 0;
            _activeTurnable = "None";
            _runner = StartCoroutine(runner());
        }

        public override void StopController()
        {
            D.Trace("[TurnController] StopController");
            StopCoroutine(_runner);
        }

        private void OnEnable()
        {
            D.Trace("[TurnController] OnEnable");
        }

        public void RegisterTurnable(Turnable turnable)
        {
            D.Trace("[TurnController] RegisterTurnable");
            _turnables.Add(turnable);
            turnable.Registered();
        }

        private IEnumerator runner()
        {
            D.Trace("[TurnController] runner");
            while (true)
            {
                yield return StartCoroutine(nextTurn());

                if (!Player.IsAlive)
                {
                    GameManager.Instance.Lives -= 1;
                    GameManager.Instance.GameController.PlayerDied();
                    break;
                }

                if (GameManager.Instance.GameController.CollectedResources.Count == GameManager.Instance.GameController.World.TotalResources)
                {
                    GameManager.Instance.GameController.NextWorld();
                    break;
                }
            }
        }

        private IEnumerator nextTurn()
        {
            D.Trace("[TurnController] nextTurn");
            _turnNumber += 1;

            //  player turn

            _activeTurnable = "Player";
            yield return StartCoroutine(Player.BeforeTurn());
            yield return StartCoroutine(Player.Turn());
            yield return StartCoroutine(Player.AfterTurn());

            foreach (Turnable turnable in _turnables)
            {
                _activeTurnable = turnable.name;
                yield return StartCoroutine(turnable.BeforeTurn());
                yield return StartCoroutine(turnable.Turn());
                yield return StartCoroutine(turnable.AfterTurn());
            }
        }
    }
}