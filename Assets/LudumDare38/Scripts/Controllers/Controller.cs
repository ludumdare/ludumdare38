﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Controllers
{
    public class Controller : MonoBehaviour
    {
        public virtual void InitController()
        {
            D.Trace("[Controller] InitController");
        }
        public virtual void StartController()
        {
            D.Trace("[Controller] StartController");
        }

        public virtual void StopController()
        {
            D.Trace("[Controller] StopController");
        }

    }
}