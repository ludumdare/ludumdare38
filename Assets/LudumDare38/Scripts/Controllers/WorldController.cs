﻿//using CreativeSpore.SuperTilemapEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Brushes;
using LudumDare.Game;
using CreativeSpore.SuperTilemapEditor;

namespace LudumDare.Controllers
{
    public class WorldTile
    {
        public int Xposition { get; set; }
        public int YPosition { get; set; }
        public Game.TileObject Tile { get; set; }

        public Vector2 GetWorldPosition()
        {
            //Vector2 pos = new Vector2( (x * 16.0f) + 8.0f, (y * 16.0f) + 8.0f );
            Vector2 pos = new Vector2((Xposition) + 8.0f, (YPosition) + 8.0f);
            return pos;
        }
    }

    public class WorldController : Controller
    {
        Dictionary<Vector2, WorldTile> _worldTiles;
        List<Brush> _obstacleBrushes;
        List<Brush> _resourceBrushes;
        List<Brush> _gameBrushes;
        List<Brush> _itemBrushes;

        public IEnumerable<WorldTile> GetTiles()
        {
            foreach (WorldTile tile in _worldTiles.Values)
            {
                yield return tile;
            }
        }

        public WorldTile GetTile(Vector2 pos)
        {
            Vector2 key = pos;
            if (_worldTiles.ContainsKey(key))
            {
                return _worldTiles[key];
            }
            return null;
            
        }

        public int TotalResources
        {
            get { return _resourceBrushes.Count; }
        }

        public Vector3 WorldCenterPosition
        {
            get { return GetComponent<TilemapGroup>().Tilemaps [0].MapBounds.center + transform.position; }
        }

        public override void StartController()
        {
            D.Trace("[WorldController] StartController");
            buildPlayfield();
            startBrushes(_obstacleBrushes);
            startBrushes(_itemBrushes);
            startBrushes(_resourceBrushes);
            startBrushes(_gameBrushes);
        }

        public override void StopController()
        {
            D.Trace("[WorldController] StopController");
            destroyPlayfield();
            stopBrushes(_gameBrushes);
        }

        private void destroyPlayfield()
        {

        }

        //  from the tiles, create a playfield
        private void buildPlayfield()
        {
            D.Trace("[WorldController] buildPlayfield");
            /*
            */

            _worldTiles = new Dictionary<Vector2, WorldTile>();

            /*
            Tilemap tiles = transform.FindChild("Ground").GetComponent<Tilemap>();

            for (int x = tiles.MinGridX; x <= tiles.MaxGridX; x++)
            {
                for(int y = tiles.MinGridY; y <= tiles.MaxGridY; y++)
                {
                    Tile tile = tiles.GetTile(new Vector2(x, y));
                    uint rawTiledata = tiles.GetTileData(new Vector2(x, y));
                    TileData data = new TileData(rawTiledata);
                    WorldTile worldTile = new WorldTile();
                    worldTile.IsAvailable = !data.IsEmpty;
                    D.Log(string.Format("X: {0}, Y: {1}, Empty: {2}, Id: {3}, Brush: {4}", x, y, data.IsEmpty, data.tileId, data.brushId));
                    worldTile.x = x;
                    worldTile.y = y;
                    _worldTiles.Add(new Vector2(x, y), worldTile);
                    if (worldTile.IsAvailable)
                        Instantiate(Test, worldTile.GetWorldPosition(), Quaternion.identity);
                }
            }
            */

            Transform t = transform.Find("Ground");

            foreach (Transform tf in t)
            {
                D.Fine("- child:" + tf.gameObject.name);
                if (tf.gameObject.activeSelf)
                {
                    WorldTile worldTile = new WorldTile();
                    worldTile.Tile = new Game.TileObject
                    {
                        IsWalkable = true,
                    };
                    worldTile.Xposition = Mathf.FloorToInt(tf.transform.position.x);
                    worldTile.YPosition = Mathf.FloorToInt(tf.transform.position.y);
                    D.Fine(string.Format("- X: {0}, Y: {1}", worldTile.Xposition, worldTile.YPosition));
                    if (!_worldTiles.ContainsKey(new Vector2(worldTile.Xposition, worldTile.YPosition)))
                        _worldTiles.Add(new Vector2(worldTile.Xposition, worldTile.YPosition), worldTile);
                }

            }

            _obstacleBrushes = new List<Brush>();
            _itemBrushes = new List<Brush>();
            _resourceBrushes = new List<Brush>();
            _gameBrushes = new List<Brush>();

            t.position = t.position + (Vector3.down * 5.0f);

            //  get any obstacles
            _obstacleBrushes = getBrushes("Obstacles", Vector2.zero);

            //  get any items
            _itemBrushes = getBrushes("Items", Vector2.zero);

            //  get any resources
            _resourceBrushes= getBrushes("Resources", Vector2.zero);

            //  get any game elements
            _gameBrushes = getBrushes("Game", Vector2.zero);

        }

        private List<Brush> getBrushes(string parent, Vector2 offset)
        {
            D.Trace("[WorldController] getBrushes");
            List<Brush> brushes = new List<Brush>();

            Transform t = transform.Find(parent);

            foreach (Transform tf in t)
            {
                D.Fine("- child:" + tf.gameObject.name);
                Brush brush = tf.GetComponent<Brush>();

                if (brush != null)
                {
                    brush.World = this;
                    int x = Mathf.FloorToInt(tf.transform.position.x);
                    int y = Mathf.FloorToInt(tf.transform.position.y);
                    Vector2 tileKey = new Vector2(x, y) + offset;
                    brush.WorldTile = GetTile(tileKey);
                    brushes.Add(brush);
                }
            }
            return brushes;
        }

        private void startBrushes(List<Brush> brushes)
        {
            D.Trace("[WorldController] startBrushes");
            foreach (Brush brush in brushes)
            {
                brush.StartBrush();
            }
        }

        private void stopBrushes(List<Brush> brushes)
        {
            D.Trace("[WorldController] stopBrushes");
            foreach (Brush brush in brushes)
            {
                brush.StopBrush();
            }
        }

    }
}