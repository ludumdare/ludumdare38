﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using LudumDare.Managers;
using LudumDare.Brushes;
using LudumDare.Game;
using DG.Tweening;

namespace LudumDare.Controllers
{
    public class GameController : Controller
    {
        public Text MessageText;
        public SystemController [] Systems;
        public GameObject MinedObject;

        public ShipStart Ship { get; set; }

        private GameManager _gameManager;



        private SystemController _system;
        private WorldController _world;
        private PlayerController _player;
        private TurnController _turn;
        private Camera _camera;
        private GameObject __mined_root__;

        public List<ResourceObject> CollectedResources;

        public SystemController System
        {
            get { return _system; }
        }

        public TurnController Turn
        {
            get { return _turn; }
        }

        public PlayerController Player
        {
            get { return _player; }
        }

        public WorldController World
        {
            get { return _world; }
        }

        public void StartWorld(int world)
        {
            _world = _system.GetWorld(world);
            StartCoroutine(flashMessage("ready   player   one"));
            StartCoroutine(startWorld());
        }

        private IEnumerator flashMessage(string message)
        {
            for (int i = 1; i < 8; i++)
            {
                MessageText.text = message;
                yield return new WaitForSeconds(0.8f / i);
                MessageText.text = "";
                yield return new WaitForSeconds(0.6f / i);
            }
            yield return null;
        }

        public void ReplayWorld()
        {
            D.Trace("[GameController] ReplayWorld");
            GameHelper.ReloadCurrentScene();
        }

        public void PlayerDied()
        {
            D.Trace("[GameController] PlayerDied");
            if (_gameManager.Lives <= 0)
            {
                _gameManager.GameState = GameStates.GAME_OVER;
                GameHelper.ReloadCurrentScene();
                return;
            }

            StartCoroutine(playerDied());
        }

        private IEnumerator playerDied()
        {
            yield return transform.DOShakeRotation(2.0f).WaitForCompletion();
            ReplayWorld();
            yield return null;
        }

        private IEnumerator replayWorld()
        {
            D.Trace("[GameController] replayWorld");
            yield return StartCoroutine(stopWorld());
            yield return StartCoroutine(startWorld());
            yield return null;
        }

        public void NextWorld()
        {
            D.Trace("[GameController] NextWorld");
            StartCoroutine(nextWorld());
        }

        public void AddMined(Vector2 pos)
        {
            GameObject go = Instantiate(MinedObject, pos + (Vector2.down * 5.0f), Quaternion.identity);
            go.transform.parent = __mined_root__.transform;
        }

        private IEnumerator nextWorld()
        {
            D.Trace("[GameController] nextWorld");
            yield return StartCoroutine(Ship.WorldCompleted());
            yield return StartCoroutine(stopWorld());

            if (!_system.SystemComplete)
            {
                _gameManager.CurrentWorld += 1;
                _world = _system.GetWorld(_gameManager.CurrentWorld);
                yield return StartCoroutine(startWorld());
            }
            else
            {
                _gameManager.GameState = GameStates.GAME_WIN;
                yield return StartCoroutine(stopSystem());
                GameHelper.ReloadCurrentScene();
            }
            yield return null;
        }

        private IEnumerator stopWorld()
        {
            D.Trace("[GameController] stopWorld");
            _player.StopController();
            _world.StopController();
            _turn.StopController();
            yield return null;
        }

        private IEnumerator startWorld()
        {
            D.Trace("[GameController] startWorld");
            CollectedResources = new List<ResourceObject>();
            _camera.transform.position = _world.WorldCenterPosition + (Vector3.left * 40.0f);
            _turn.InitController();
            _world.StartController();
            _player.StartController();
            _player.Player.Ship = Ship;
            _turn.StartController();
            yield return null;
        }

        private IEnumerator stopSystem()
        {
            D.Trace("[GameController] stopSystem");
            yield return null;
        }

        public override void StartController()
        {
            D.Trace("[GameController] StartController");
            _camera = Camera.main;
            _system = Systems [_gameManager.CurrentSystem];
            _turn = GameObject.FindObjectOfType<TurnController>();
            _player = GameObject.FindObjectOfType<PlayerController>();
            _turn.Player = _player.Player;
            _system.StartController();
        }

        public override void StopController()
        {
            D.Trace("[GameController] EndController");
            _player.StopController();
            _world.StopController();
            _turn.StopController();
            _system.StopController();
        }

        private void OnEnable()
        {
            _gameManager = GameManager.Instance;
            _gameManager.GameController = this;
            __mined_root__ = new GameObject("__mined_root__");
        }

    }
}