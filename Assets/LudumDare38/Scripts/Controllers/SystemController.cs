﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Controllers
{
    public class SystemController : Controller
    {
        public WorldController[] Worlds;

        public int CurrentWorld { get; set; }

        public bool SystemComplete
        {
            get
            {
                if (CurrentWorld == Worlds.Length-1)
                    return true;

                return false;
            }
        }

        public WorldController GetWorld(int index)
        {
            D.Trace("[SystemController] GetWorld");
            if (index > Worlds.Length)
                return null;

            WorldController wc = Worlds [index];
            CurrentWorld = index;
            return wc;
        }

        public override void StartController()
        {
            D.Trace("[SystemController] StartController");
        }

        public override void StopController()
        {
            D.Trace("[SystemController] EndController");
        }

        public WorldController NextWorld()
        {
            D.Trace("[SystemController] NextWorld");
            CurrentWorld += 1;
            WorldController world = Worlds [CurrentWorld];
            return world;
        }

    }
}