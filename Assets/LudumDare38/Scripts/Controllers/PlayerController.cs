﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game;

namespace LudumDare.Controllers
{
    public class PlayerController : Controller
    {
        private Player _player;

        public Player Player
        {
            get { return _player; }
        }

        public override void StartController()
        {
            D.Trace("[PlayerController] StartController");
            _player.Reset();
        }

        public override void StopController()
        {
            D.Trace("[PlayerController] EndController");
            _player.Reset();
        }

        private void OnEnable()
        {
            D.Trace("[PlayerController] OnEnable");
            _player = GetComponent<Player>();
        }
    }
}