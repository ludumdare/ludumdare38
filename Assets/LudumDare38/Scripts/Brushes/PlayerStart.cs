﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Managers;

namespace LudumDare.Brushes
{
    public class PlayerStart : Brush
    {
        public override void StartBrush()
        {
            D.Trace("[PlayerStart] StartBrush");
            GameManager.Instance.Player.transform.position = transform.position;
            GetComponent<SpriteRenderer>().enabled = false;
        }

    }
}