﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Managers;
using LudumDare.Game;

namespace LudumDare.Brushes
{
    public class AlienStart : Brush
    {
        public Alien AlienObject;

        public override void StartBrush()
        {
            D.Trace("[SlitherStart] StartBrush");
            GameObject go = Instantiate(AlienObject.gameObject, transform.position, Quaternion.identity);
            GetComponent<SpriteRenderer>().enabled = false;
            GameManager.Instance.GameController.Turn.RegisterTurnable(go.GetComponent<Alien>());
        }
    }
}