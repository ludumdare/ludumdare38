﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Managers;
using LudumDare.Game;

namespace LudumDare.Brushes
{
    public class ShipStart : Brush
    {
        public override void StartBrush()
        {
            D.Trace("[ShipStart] StartBrush");
            GameManager.Instance.GameController.Ship = this;
        }

        public IEnumerator WorldCompleted()
        {
            D.Trace("[ShipStart] StartBrush");
            yield return null;
        }
    }
}