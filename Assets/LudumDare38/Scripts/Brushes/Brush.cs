﻿using LudumDare.Controllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Brushes
{
    public class Brush : MonoBehaviour
    {
        public WorldController World { get; set; }
        public WorldTile WorldTile { get; set; }

        public virtual void StartBrush()
        {
            D.Trace("[Brush] StartBrush");
        }

        public virtual void StopBrush()
        {
            D.Trace("[Brush] StopBrush");
        }
    }
}