﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game.ObstacleObjects;

namespace LudumDare.Brushes.Obstacles
{
    public class RockBrush : Brush
    {
        public override void StartBrush()
        {
            D.Trace("[RockBrush] StartBrush");
            //transform.Translate(Vector2.up * 5.0f);
            WorldTile.Tile.SurfaceObject = new Rock();
        }
    }
}