﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game.ObstacleObjects;

namespace LudumDare.Brushes.Obstacles
{
    public class PitBrush : Brush
    {
        public override void StartBrush()
        {
            D.Trace("[PitBrush] StartBrush");
            transform.Translate(Vector2.down * 5.0f);
            WorldTile.Tile.SurfaceObject = new Pit();
        }
    }
}