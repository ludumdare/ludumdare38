﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game.ResourceObjects;

namespace LudumDare.Brushes.Resource
{
    public class GreenCrystalBrush : Brush
    {
        public override void StartBrush()
        {
            D.Trace("[GreenCrystalBrush] StartBrush");
            WorldTile.Tile.ResourceObject = new GreenCrystal();
            WorldTile.Tile.ResourceObject.ResourceGameObject = this.gameObject;
            WorldTile.Tile.ResourceObject.Hide();
        }
    }
}