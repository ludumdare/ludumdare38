﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game.ResourceObjects;

namespace LudumDare.Brushes.Resource
{
    public class PurpleCrystalBrush : Brush
    {
        public override void StartBrush()
        {
            D.Trace("[PurpleCrystalBrush] StartBrush");
            WorldTile.Tile.ResourceObject = new PurpleCrystal();
            WorldTile.Tile.ResourceObject.ResourceGameObject = this.gameObject;
            WorldTile.Tile.ResourceObject.Hide();
        }
    }
}