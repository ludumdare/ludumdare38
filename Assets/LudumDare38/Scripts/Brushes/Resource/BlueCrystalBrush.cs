﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game.ResourceObjects;

namespace LudumDare.Brushes.Resource
{
    public class BlueCrystalBrush: Brush
    {
        public override void StartBrush()
        {
            D.Trace("[BlueCrystalBrush] StartBrush");
            WorldTile.Tile.ResourceObject = new BlueCrystal();
            WorldTile.Tile.ResourceObject.ResourceGameObject = this.gameObject;
            WorldTile.Tile.ResourceObject.Hide();
        }
    }
}