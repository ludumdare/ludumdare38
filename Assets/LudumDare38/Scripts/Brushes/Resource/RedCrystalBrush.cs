﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game.ResourceObjects;

namespace LudumDare.Brushes.Resource
{
    public class RedCrystalBrush : Brush
    {
        public override void StartBrush()
        {
            D.Trace("[RedCrystalBrush] StartBrush");
            WorldTile.Tile.ResourceObject = new RedCrystal();
            WorldTile.Tile.ResourceObject.ResourceGameObject = this.gameObject;
            WorldTile.Tile.ResourceObject.Hide();
        }
    }
}