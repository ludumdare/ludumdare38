﻿using UnityEngine;

namespace LudumDare.Data
{
    [CreateAssetMenu]
    [System.Serializable]
    public class GameSettingsData : ScriptableObject
    {
        [SerializeField]
        public KeyCode ControlLeft;
        [SerializeField]
        public KeyCode ControlRight;
        [SerializeField]
        public KeyCode ControlUp;
        [SerializeField]
        public KeyCode ControlDown;
        [SerializeField]
        public KeyCode ControlAction;
        [SerializeField]
        public bool FullScreen;     //  is the game fullscreen?
        [SerializeField]
        public int ScreenSize;      //  what is the size of the game screen (1x, 2x, 3x)?
        public int LastSave;        //  last save position (1-3)
    }
}
