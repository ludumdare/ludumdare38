﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Data
{
    [System.Serializable]
    public class PlayerSettingsData
    {
        public int LastSystem;
        public int LastWorld;
        public int Score;
    }
}