﻿using LudumDare.Controllers;
using LudumDare.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Game
{
    public class PlayerTarget : MonoBehaviour
    {
        public bool IsActive { get; set; }
        public TileObject CurrentTile { get; set; }

        public void SetTargetHorizontal(Player player)
        {
            Vector2 pos = player.transform.position;
            Vector2 newpos = pos;

            IsActive = true;

            if (player.FacingLeft)
            {
                newpos = pos + (Vector2.right * 16.0f);
                WorldTile tile = GetTile(newpos);
                if (tile == null)
                {
                    IsActive = false;
                    newpos = pos;
                }
                else
                {
                    if (tile.Tile.HasSurfaceObject)
                    {
                        if (!tile.Tile.SurfaceObject.IsPassable)
                        {
                            IsActive = false;
                            newpos = pos;
                        }
                    }
                }
            }

            if (!player.FacingLeft)
            {
                newpos = pos + (Vector2.left  * 16.0f);
                WorldTile tile = GetTile(newpos);
                if (tile == null)
                {
                    IsActive = false;
                    newpos = pos;
                }
                else
                {
                    if (tile.Tile.HasSurfaceObject)
                    {
                        if (!tile.Tile.SurfaceObject.IsPassable)
                        {
                            IsActive = false;
                            newpos = pos;
                        }
                    }
                }
            }

            transform.position = newpos;
            CurrentTile = GetTile(newpos).Tile;
        }

        public void SetTargetVertical(Player player)
        {
            Vector2 pos = player.transform.position;
            Vector2 newpos = pos;

            IsActive = true;

            if (player.FacingUp)
            {
                newpos = pos + (Vector2.down * 16.0f);
                WorldTile tile = GetTile(newpos);
                if (tile == null)
                {
                    IsActive = false;
                    newpos = pos;
                }
                else
                {
                    if (tile.Tile.HasSurfaceObject)
                    {
                        if (!tile.Tile.SurfaceObject.IsPassable)
                        {
                            IsActive = false;
                            newpos = pos;
                        }
                    }
                }
            }

            if (!player.FacingUp)
            {
                newpos = pos + (Vector2.up * 16.0f);
                WorldTile tile = GetTile(newpos);
                if (tile == null)
                {
                    IsActive = false;
                    newpos = pos;
                }
                else
                {
                    if (tile.Tile.HasSurfaceObject)
                    {
                        if (!tile.Tile.SurfaceObject.IsPassable)
                        {
                            IsActive = false;
                            newpos = pos;
                        }
                    }
                }
            }

            transform.position = newpos;
            CurrentTile = GetTile(newpos).Tile;
        }

        private WorldTile GetTile(Vector2 position)
        {
            return GameManager.Instance.GameController.World.GetTile(position);
        }
    }
}