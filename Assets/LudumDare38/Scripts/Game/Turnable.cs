﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare;

namespace LudumDare.Game
{
    public class Turnable : Moveable
    {
        public virtual void  Reset()
        {
            D.Trace("[Turnable] Reset");
        }

        public virtual IEnumerator BeforeTurn()
        {
            D.Trace("[Turnable] StartTurn");
            yield return null;
        }

        public virtual IEnumerator Turn()
        {
            D.Trace("[Turnable] StartTurn");
            yield return null;
        }

        public virtual IEnumerator AfterTurn()
        {
            D.Trace("[Turnable] StartTurn");
            yield return null;
        }

        public virtual void Registered()
        {
            D.Trace("[Turnable] Registered");
        }
    }
}