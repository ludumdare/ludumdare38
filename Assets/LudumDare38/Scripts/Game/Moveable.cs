﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Controllers;
using LudumDare.Managers;

namespace LudumDare.Game
{
    public enum MoveActions
    {
        MOVE_LEFT,
        MOVE_RIGHT,
        MOVE_UP,
        MOVE_DOWN,
        MOVE_ACTION
    }

    public class Moveable : MonoBehaviour
    {


        public TileObject CurrentTile { get; set; }
        public TileObject PreviousTile { get; set; }

        private float  _facingLeft;

        public void MoveLeft()
        {
            D.Trace("[Moveable] MoveLeft");
            _facingLeft = 1.0f;
            move(Vector2.left);
        }

        public void MoveRight()
        {
            D.Trace("[Moveable] MoveRight");
            _facingLeft = -1.0f;
            move(Vector2.right);
        }

        public void MoveUp()
        {
            D.Trace("[Moveable] MoveUp");
            move(Vector2.up);
        }

        public void MoveDown()
        {
            D.Trace("[Moveable] MoveDown");
            move(Vector2.down);
        }

        private void move(Vector2 dir)
        {
            D.Trace("[Moveable] move");
            transform.localScale = new Vector2(_facingLeft, 1);
            transform.Translate(dir * 16.0f);
            WorldTile worldTile = GameManager.Instance.GameController.World.GetTile(transform.position);
            PreviousTile = CurrentTile;
            CurrentTile = worldTile.Tile;
        }

        public void Action()
        {
            D.Trace("[Moveable] Action");
        }

        public bool CanMove(Vector2 moveAction)
        {
            D.Trace("[Moveable] CanMove");

            bool move = true;

            Vector2 newpos = (Vector2)transform.position + moveAction * 16.0f;

            WorldTile worldTile = GameManager.Instance.GameController.World.GetTile(newpos);

            if (worldTile != null)
            {
                if (worldTile.Tile.HasSurfaceObject)
                {
                    if (!worldTile.Tile.SurfaceObject.IsPassable)
                        move = false;
                }
            }
            else
            {
                move = false;
            }

            return move;
        }
    }
}