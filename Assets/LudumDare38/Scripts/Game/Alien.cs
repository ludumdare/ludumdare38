﻿using LudumDare.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Game
{
    public class Alien : Actor
    {
        public override IEnumerator Turn()
        {
            D.Trace("[Alien] Turn");

            int r = Random.Range(0, 4);

            if (r == 0)
            {
                if (CanMove(Vector2.up))
                {
                    MoveUp();
                }
            }

            if (r == 1)
            {
                if (CanMove(Vector2.down))
                {
                    MoveDown();
                }
            }

            if (r == 2)
            {
                if (CanMove(Vector2.left))
                {
                    MoveLeft();
                }
            }

            if (r == 3)
            {
                if (CanMove(Vector2.right))
                {
                    MoveRight();
                }
            }

            return base.Turn();
        }

        public override IEnumerator AfterTurn()
        {
            if (GameManager.Instance.Player.transform.position == transform.position)
            {
                GameManager.Instance.Player.IsAlive = false;
            }
            return base.AfterTurn();
        }
    }

}