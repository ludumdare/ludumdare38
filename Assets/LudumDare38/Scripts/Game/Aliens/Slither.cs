﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game;

namespace LudumDare.Game.Aliens
{
    public class Slither : Alien
    {
        public override IEnumerator BeforeTurn()
        {
            D.Trace("[Slither] StartTurn");
            yield return null;
        }
    }
}