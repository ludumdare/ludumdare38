﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Game
{
    public class TileObject
    {
        public bool IsWalkable { get; set; }
        public SurfaceObject SurfaceObject { get; set; }
        public ResourceObject ResourceObject { get; set; }

        public bool HasSurfaceObject
        {
            get
            {
                if (SurfaceObject == null)
                    return false;

                return true;
            }
        }

        public bool HasResourceObject
        {
            get
            {
                if (ResourceObject == null)
                    return false;

                return true;
            }
        }
    }
}