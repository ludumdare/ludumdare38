﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Managers;
using LudumDare.Controllers;
using LudumDare.Brushes;

namespace LudumDare.Game
{
    public class Player : Actor
    {
        public PlayerTarget Target;

        public ResourceObject CurrentResource { get; set; }
        public ShipStart Ship { get; set; }

        private bool _facingLeft;
        private bool _facingUp;
        private bool _movedVertical;

        public bool FacingLeft
        {
            get { return _facingLeft; }
        }

        public bool FacingUp
        {
            get { return _facingUp; }
        }

        public override void Reset()
        {
            D.Trace("[Player] Reset");
            IsAlive = true;
            IsConfused = false;
            IsFrozen = false;
            Target.SetTargetHorizontal(this);
            CurrentResource = null;
            CurrentTile = null;
        }

        public override IEnumerator BeforeTurn()
        {
            D.Trace("[Player] BeforeTurn");
            yield return null;
        }

        public override IEnumerator Turn()
        {
            D.Trace("[Player] Turn");

            while (true)
            {
                yield return null;

                if (IsFrozen)
                    continue;

                if (IsConfused)
                    continue;

                if (Input.GetKeyDown(GameManager.Instance.GameSettings.ControlLeft))
                {
                    if (CanMove(Vector2.left))
                    {
                        MoveLeft();
                        _facingLeft = true;
                        _movedVertical = false;
                        break;
                    }
                }

                if (Input.GetKeyDown(GameManager.Instance.GameSettings.ControlRight))
                {
                    if (CanMove(Vector2.right))
                    {
                        MoveRight();
                        _facingLeft = false;
                        _movedVertical = false;
                        break;
                    }
                }

                if (Input.GetKeyDown(GameManager.Instance.GameSettings.ControlUp))
                {
                    if (CanMove(Vector2.up))
                    {
                        MoveUp();
                        _facingUp = true;
                        _movedVertical = true;
                        break;
                    }
                }

                if (Input.GetKeyDown(GameManager.Instance.GameSettings.ControlDown))
                {
                    if (CanMove(Vector2.down))
                    {
                        MoveDown();
                        _facingUp = false;
                        _movedVertical = true;
                        break;
                    }
                }

                if (Input.GetKeyDown(GameManager.Instance.GameSettings.ControlAction))
                {
                    if (Target.IsActive)
                    {
                        if (Target.CurrentTile.HasResourceObject)
                        {
                            if (!Target.CurrentTile.ResourceObject.Mined)
                            {
                                SoundManager.Instance.PlaySound("Mining");
                                Target.CurrentTile.ResourceObject.Mined = true;
                                GameManager.Instance.GameController.AddMined(Target.gameObject.transform.position);
                            }
                        } else
                        {
                            SoundManager.Instance.PlaySound("Mining");
                            GameManager.Instance.GameController.AddMined(Target.gameObject.transform.position);
                        }
                    }


                    if (CurrentResource != null)
                    {   
                        //  drop resource
                        if (!Target.CurrentTile.HasResourceObject)
                        {
                            WorldTile worldTile = GameManager.Instance.GameController.World.GetTile(Target.transform.position);
                            if (worldTile != null)
                            {
                                SoundManager.Instance.PlaySound("DropResource");
                                worldTile.Tile.ResourceObject = CurrentResource;
                                worldTile.Tile.ResourceObject.ResourceGameObject.GetComponent<SpriteRenderer>().sortingOrder = 0;
                                CurrentResource = null;
                            }
                        } else
                        //  swap resource
                        {
                            WorldTile worldTile = GameManager.Instance.GameController.World.GetTile(Target.transform.position);
                            if (worldTile != null)
                            {
                                ResourceObject newResource = worldTile.Tile.ResourceObject;
                                worldTile.Tile.ResourceObject = CurrentResource;
                                worldTile.Tile.ResourceObject.ResourceGameObject.GetComponent<SpriteRenderer>().sortingOrder = 0;
                                CurrentResource = newResource;
                                SoundManager.Instance.PlaySound("SwapResource");
                            }
                        }
                    }
                    break;
                }



            }
            yield return null;
        }

        public override IEnumerator AfterTurn()
        {
            if (!_movedVertical)
                Target.SetTargetHorizontal(this);

            if (_movedVertical)
                Target.SetTargetVertical(this);

            if (Target.CurrentTile != null)
            {
                if (Target.CurrentTile.HasResourceObject)
                {
                    if (Target.CurrentTile.ResourceObject.Mined)
                        Target.CurrentTile.ResourceObject.Show();
                }
            }

            if (CurrentResource == null)
            {
                if (CurrentTile != null)
                {
                    if (CurrentTile.HasResourceObject)
                    {
                        if (CurrentTile.ResourceObject.Mined)
                        {
                            SoundManager.Instance.PlaySound("PickupResource");
                            CurrentResource = CurrentTile.ResourceObject;
                            CurrentTile.ResourceObject = null;
                        }
                    }
                }
            }

            if (CurrentResource != null)
            {
                CurrentResource.ResourceGameObject.transform.position = Target.transform.position;
                CurrentResource.ResourceGameObject.GetComponent<SpriteRenderer>().sortingOrder = 99;
                Target.IsActive = false;

                if (transform.position == Ship.transform.position)
                {
                    SoundManager.Instance.PlaySound("CollectResource");
                    CurrentResource.Hide();
                    GameManager.Instance.GameController.CollectedResources.Add(CurrentResource);
                    CurrentResource = null;
                }
            }


            return base.AfterTurn();
        }

        private void Start()
        {
            GameManager.Instance.Player = this;
        }
    }
}