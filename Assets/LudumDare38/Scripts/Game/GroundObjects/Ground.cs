﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game;

namespace LudumDare.Game.GroundObjects
{
    public class Ground : GroundObject
    {

        public Ground()
        {
            IsPassable = true;
        }
    }
}