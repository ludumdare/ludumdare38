﻿namespace LudumDare.Game.ResourceObjects
{
    public class RedCrystal : ResourceObject
    {
        public RedCrystal()
        {
            Value = 100;
        }

    }
}