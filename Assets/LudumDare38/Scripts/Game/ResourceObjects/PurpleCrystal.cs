﻿namespace LudumDare.Game.ResourceObjects
{
    public class PurpleCrystal : ResourceObject
    {
        public PurpleCrystal()
        {
            Value = 100;
        }

    }
}