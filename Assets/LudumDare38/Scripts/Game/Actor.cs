﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Game
{
    public class Actor : Turnable
    {
        public bool IsAlive { get; set; }
        public bool IsFrozen { get; set; }
        public bool IsConfused { get; set; }

        public override IEnumerator AfterTurn()
        {
            D.Trace("[Actor] AfterTurn");

            if (CurrentTile != null)
            {
                if (CurrentTile.HasSurfaceObject)
                {
                    CurrentTile.SurfaceObject.AfterTurn(this);
                }
            }

            yield return null;
        }
    }
}