﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game.GroundObjects;

namespace LudumDare.Game.ObstacleObjects
{
    public class Rock : SurfaceObject
    {
        public Rock()
        {
            IsDestructible = true;
        }
    }
}