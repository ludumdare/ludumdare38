﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game.GroundObjects;

namespace LudumDare.Game.ObstacleObjects
{
    public class Pit : SurfaceObject
    {
        public Pit()
        {
            IsPassable = true;
            IsDeadly = true;
        }

        public override void AfterTurn(Actor actor)
        {
            D.Trace("[Pit] AfterTurn");
            actor.IsAlive = false;
        }
    }
}