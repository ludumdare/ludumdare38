﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Game.GroundObjects;

namespace LudumDare.Game.ObstacleObjects
{
    public class Tree : SurfaceObject
    {
        public Tree()
        {
            IsDestructible = true;
        }
    }
}