﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Game
{
    public class SurfaceObject
    {
        public bool IsPassable { get; set; }
        public bool IsSwapable { get; set; }
        public bool IsCollectible { get; set; }
        public bool IsDestructible { get; set; }
        public bool IsDroppable { get; set; }
        public bool IsDeadly { get; set; }
        public int Value { get; set; }

        //  what to do when actor ends turn on me
        public virtual void AfterTurn(Actor actor)
        {
            D.Trace("[TileObject] AfterTurn");
        }
    }
}