﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Game
{
    public class ResourceObject
    {
        public bool Mined { get; set; }
        public bool Collected { get; set; }
        public int Value { get; set; }
        public GameObject ResourceGameObject { get; set; }

        public virtual void Show()
        {
            D.Trace("[ResourceObject] Show");
            ResourceGameObject.GetComponent<SpriteRenderer>().enabled = true;
        }

        public virtual void Hide()
        {
            D.Trace("[ResourceObject] Hide");
            ResourceGameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

    }
}