﻿using UnityEngine;
using System.Collections.Generic;
using LudumDare.Managers;

namespace LudumDare.Controllers
{
    public class SoundManager : MonoSingleton<SoundManager>
    {
        public AudioClip[] Clips;

        private Dictionary<string, AudioClip> _clips;

        private List<GameObject> soundLoops;

        private float _volume = 1.0f;

        public void StopSound()
        {
            foreach (GameObject go in soundLoops)
            {
                Destroy(go.gameObject);
            }
        }

        public GameObject PlaySound(string name, bool loop = false)
        {
            if (loop == false)
            {
                return playPitchSound(_clips[name]);
            }
            else
            {
                return playSoundLoop(_clips[name]);
            }
        }

        public void SetVolume(float volume)
        {
            _volume = volume;
        }

        //	PRIVATE

        private GameObject playPitchSound(AudioClip clip, float range = 0.0f)
        {
            D.Trace("[GameSounds] playPitchSound");
            return playSound(clip, 1.0f + Random.Range(-range, range), _volume);
        }

        private GameObject playSound(AudioClip clip)
        {

            D.Trace("[GameSounds] playSound");
            return playSound(clip, 1.0f, _volume);
        }

        private GameObject playSound(AudioClip clip, float pitch, float volume)
        {

            D.Trace("[GameSounds] playSound");
            D.Detail("- _volume is " + _volume);
            D.Detail("- volume is " + volume);
            GameObject go = new GameObject(string.Format("[Temp] Sound ({0})", clip.name));
            DontDestroyOnLoad(go);
            AudioSource gs = go.AddComponent<AudioSource>();
            gs.clip = clip;
            gs.volume = volume;
            gs.pitch = pitch;
            gs.Play();
            Destroy(go, clip.length + 0.1f);
            return go;
        }

        private GameObject playSoundLoop(AudioClip clip)
        {

            D.Trace("[GameSounds] playSound");
            //audio.pitch = pitch;
            //audio.PlayOneShot(clip, volume);
            //	reset
            //audio.pitch = 1.0f;

            GameObject go = new GameObject("Sound (Temp)");
            DontDestroyOnLoad(go);
            AudioSource gs = go.AddComponent<AudioSource>();
            gs.clip = clip;
            gs.loop = true;
            gs.Play();
            soundLoops.Add(go);
            return go;
        }

        //  MONO

        private void Start()
        {
            D.Trace("[SoundController] Start");
            foreach (AudioClip clip in Clips)
            {
                _clips.Add(clip.name, clip);
            }
        }

        private void OnEnable()
        {
            D.Trace("[SoundManager] OnEnable");
            soundLoops = new List<GameObject>();
            _clips = new Dictionary<string, AudioClip>();
            DontDestroyOnLoad(gameObject);
        }

    }
}