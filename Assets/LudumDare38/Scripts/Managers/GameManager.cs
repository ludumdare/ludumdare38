﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Data;
using LudumDare.Game;
using LudumDare.Factories;
using LudumDare.Controllers;

namespace LudumDare.Managers
{
    public enum GameStates
    {
        GAME_HOME,
        GAME_ABOUT,
        GAME_MEDALS,
        GAME_SETTINGS,
        GAME_OVER,
        GAME_WIN,
        GAME_RUNNING
    }

    public class GameManager : MonoSingleton<GameManager>
    {
        public GameStates GameState { get; set; }
        public GameData Game { get; set; }
        public GameSettingsData GameSettings { get; set; }
        public PlayerSettingsData PlayerSettings { get; set; }
        public Player Player { get; set; }
        public GameController GameController { get; set; }
        public PrefabFactory Prefabs { get; set; }
        public int CurrentSystem { get; set; }
        public int CurrentWorld { get; set; }
        public int Lives { get; set; }
        public int Score { get; set; }
        public int HiScore { get; set; }

        public void StartNewGame()
        {
            D.Trace("[GameManager] StartNewGame");
            CurrentSystem = 0;
            CurrentWorld = 0;
            Lives = 3;
            Score = 0;
            GameState = GameStates.GAME_RUNNING;
            GameHelper.ReloadCurrentScene();
        }

        public void ContinueGame()
        {
            D.Trace("[GameManager] ContinueGame");
        }

        //  load player saved slot
        public PlayerSettingsData LoadPlayer(int position)
        {
            D.Trace("[GameManager] LoadPlayer");
            PlayerSettingsData playerData = new PlayerSettingsData();
            return playerData;
        }

        //  save player data to specified slot
        public void SavePlayer(int position)
        {

        }

        //  load game settings (will override defaults)
        public GameSettingsData LoadGameSettings(GameSettingsData gameSettings)
        {

            D.Trace("[GameManager] LoadGameSettings");
            gameSettings.ControlLeft = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("ControlLeft"));
            return gameSettings;
        }

        //  save game settings
        public void SaveGameSettings()
        {

        }

        private void OnEnable()
        {
            GameState = GameStates.GAME_HOME;
        }

    }
}