﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using LudumDare.Managers;

public class ScaleHelper : MonoBehaviour
{
    public int BaseWidth;
    public int BaseHeight;
    public Canvas[] ScaledCanvas;
    public Text ButtonText;

    public void NextScale()
    {
        D.Trace("[ScaleHelper] NextScale");
        GameManager.Instance.GameSettings.ScreenSize += 1;
        if (GameManager.Instance.GameSettings.ScreenSize > GameManager.Instance.Game.MaxSize)
            GameManager.Instance.GameSettings.ScreenSize = 1;
        scaleGame();
    }

    private void Start()
    {
        D.Trace("[ScaleHelper] NextScale");
        scaleGame();
        scaleCanvas();
    }

    private void Update()
    {
        D.Fine("[ScaleHelper] Update");
        if (Application.isEditor)
            return;
        scaleCanvas();
    }

    private void scaleCanvas()
    {
        D.Trace("[ScaleHelper] scaleCanvas");
        foreach (Canvas c in ScaledCanvas)
        {
            CanvasScaler cs = c.GetComponent<CanvasScaler>();
            cs.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
            cs.scaleFactor = (GameManager.Instance.GameSettings.ScreenSize);
            cs.referencePixelsPerUnit = 100;
        }
    }

    private void scaleGame()
    {
        D.Trace("[ScaleHelper] scaleGame");
        int scale = GameManager.Instance.GameSettings.ScreenSize;
        bool fullScreen = false;

        if (scale == 1)
        {
            ButtonText.text = "2X";
        }
        if (scale == 2)
        {
            ButtonText.text = "3X";
        }
        if (scale == 3)
        {
            ButtonText.text = "FULLSCREEN";
        }
        if (scale == 4)
        {
            ButtonText.text = "1X";
            fullScreen = true;
        }
        Screen.SetResolution(BaseWidth * scale, BaseHeight * scale, fullScreen);
    }

}
