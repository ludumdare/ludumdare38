﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LudumDare.Controllers;

public class WorkTurnController : MonoBehaviour
{
    public GameObject Test;
    public int World;
    public Text Debug;

    private GameController _game;

    public void NextWorld()
    {
        D.Trace("[WorkTurnController] NextWorld");
        _game.NextWorld();
    }

    public void RestartWorld()
    {
        D.Trace("[WorkTurnController] RestartWorld");
        _game.ReplayWorld();
    }

    public void CompleteWorld()
    {
        D.Trace("[WorkTurnController] CompleteWorld");
    }

    private void Start()
    {
        D.Trace("[WorkTurnController] Start");
        _game.StartController();
        _game.StartWorld(World);
    }

    private void Update()
    {
        if (_game.World == null)
            return;

        string debug = "";
        debug += "\nSystem: " + _game.System.name;
        debug += "\nWorld:" + _game.World.name;
        debug += "\nTurn:" + _game.Turn.TurnNumber;
        debug += "\nActive:" + _game.Turn.ActiveTurnable;
        debug += "\nAlive:" + _game.Player.Player.IsAlive;
        debug += "\nColleccted:" + _game.CollectedResources.Count;
        Debug.text = debug;
    }

    private void OnEnable()
    {
        D.Trace("[WorkTurnController] OnEnable");
        _game = GameObject.FindObjectOfType<GameController>();
    }
}
